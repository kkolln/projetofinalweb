const Sequelize = require('sequelize');
const db = require('../util/database');
const Produto = require('./produto');
const Pessoa = require('./pessoa');

const ItemCarrinho = db.define('itemCarrinho', {
    produtoId: Sequelize.INTEGER,
    pessoaId: Sequelize.INTEGER,
    qtde: Sequelize.INTEGER,
    total: Sequelize.FLOAT
});
module.exports = ItemCarrinho;