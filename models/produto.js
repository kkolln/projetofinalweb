const Sequelize = require('sequelize');
const db = require('../util/database');

const Produto = db.define('produto', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    valor: Sequelize.FLOAT,
    estoque: Sequelize.INTEGER,
    descricao: Sequelize.STRING(100),
    nome: Sequelize.STRING(45)
});

module.exports = Produto;