const Sequelize = require('sequelize');
const db = require('../util/database');

const Pessoa = db.define('pessoa', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    nome: Sequelize.STRING,
    email: Sequelize.STRING,
    senha: Sequelize.STRING
});

module.exports = Pessoa;