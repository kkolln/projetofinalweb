const ItemCarrinho = require('../models/itemCarrinho');
const Produto = require('../models/produto');

//Buscar carrinho do usuario logado
exports.getItensCarrinho = (req, res, next) => {
    const idPessoa = req.params.idPessoa;
    ItemCarrinho.findAll({
        raw: true,
        where: { pessoaId: idPessoa },        
    }).then(itemCarrinho => {
        res.status(200).json({ itensCarrinho: itemCarrinho });
    }).catch(err => console.log(err));
}

//Ao adicionar um item no carrinho o carrinho é criado
exports.addItemCarrinho = (req, res, next) => {
    const idProduto = req.body.idProduto;
    const idPessoa = req.body.idPessoa;
    const unitario = req.body.unitario;
    const qtde = req.body.qtde;
    const total = unitario * qtde;

    ItemCarrinho.create({
        produtoId: idProduto,
        pessoaId: idPessoa,
        qtde: qtde,
        total: total,
    }).then(result => {
        console.log('Item $idProduto adicionado ao carrinho!');
        res.status(201).json({
            message: 'Item $idProduto adicionado ao carrinho!',
            itemCarrinho: result
        });
    }).catch(err => console.log(err));
}

exports.updateItemCarrinho = (req, res, next) => {
    const idPessoa = req.params.idPessoa;
    const idProduto = req.params.idProduto;
    const updatedUnitario = req.body.unitario;
    const updatedQtde = req.body.qtde;
    const updatedTotal = updatedUnitario * updatedQtde;

    ItemCarrinho.findOne({
        where: {
            pessoaId: idPessoa,
            produtoId: idProduto
        }
    }).then(function (record){
        if(!record){
            return res.status(404).json({
                message: 'Produto $idProduto não encontrado no carrinho!'
            });
        }
        return record.update({qtde: updatedQtde, total: updatedTotal})
    }).then(function (record) {
        res.status(200).json({
            message: 'Produto $idProduto atualizado com sucesso!',
            itemCarrinho: record
        });
    }).catch(err => console.log(err));
}

//Exclui um unico item do carrinho
exports.deleteItemCarrinho = (req, res, next) => {
    const idProduto = req.params.idProduto;
    const idPessoa = req.params.idPessoa;
    ItemCarrinho.findOne({
        where: {
            pessoaId: idPessoa,
            produtoId: idProduto
        }
    }).then(itemCarrinho => {
        if (!itemCarrinho) {
            return res.status(404).json({
                message: 'Produto ${idProduto} não encontrado no carrinho!'
            });
        }
        return ItemCarrinho.destroy({
            where: {
                pessoaId: idPessoa,
                produtoId: idProduto
            }
        });
    }).then(result => {
        res.status(200).json({ message: 'Produto deletado do carrinho!' });
    }).catch(err => console.log(err));
}

//Exclui todos os itens do carrinho
exports.deleteItensCarrinho = (req, res, next) => {
    const idPessoa = req.params.idPessoa;
    ItemCarrinho.findAll({
        where: {
            pessoaId: idPessoa
        }
    }).then(itemCarrinho => {
        if (!itemCarrinho) {
            return res.status(404).json({
                message: 'Usuário não possui itens no carrinho!'
            });
        }
        return ItemCarrinho.destroy({
            where: {
                pessoaId: idPessoa
            }
        });
    }).then(result => {
        res.status(200).json({ message: 'Carrinho deletado' });
    }).catch(err => console.log(err));
}