const Produto = require('../models/produto');

//Buscar todos os produtos
exports.getProdutos = (req, res, next) => {
    Produto.findAll().then(produto => {
        res.status(200).json({ produto: produto });
    }).catch(err => console.log(err));
}

//Buscar um produto pelo id
exports.getProduto = (req, res, next) => {
    const id = req.params.id;
    Produto.findByPk(id).then(produto => {
        if (!produto) {
            return res.status(404).json({
                message: 'Produto não encontrado'
            });
        }
        res.status(200).json({ produto: produto });
    }).catch(err => console.log(err));
}

//Criar um produto
exports.createProduto = (req, res, next) => {
    const valor = req.body.valor;
    const estoque = req.body.estoque;
    const descricao = req.body.descricao;
    const nome = req.body.nome;

    Produto.create({
        valor: valor,
        estoque: estoque,
        descricao: descricao,
        nome: nome
    }).then(result => {
        console.log('Produto criado!');
        res.status(201).json({
            message: 'Produto cadastrado com sucesso!',
            pessoa: result
        });
    }).catch(err => {
        console.log(err);
    });
}

//Atualizar produto
exports.updateProduto = (req, res, next) => {
    const id = req.params.id;
    const updatedValor = req.body.valor;
    const updatedEstoque = req.body.estoque;
    const updatedDescricao = req.body.descricao;
    const updatedNome = req.body.nome;

    Produto.findByPk(id).then(produto => {
        if (!produto) {
            return res.status(404).json({
                message: 'Produto ${updatedNome} não encontrado!'
            });
        }
        produto.valor = updatedValor;
        produto.estoque = updatedEstoque;
        produto.descricao = updatedDescricao;
        produto.nome = updatedNome;
        return produto.save();
    }).then(result => {
        res.status(200).json({
            message: 'Produto ${updatedNome} atualizado!',
            produto: result
        });
    }).catch(err => console.log(err));
}

//Excluir um produto
exports.deleteProduto = (req, res, next) => {
    const id = req.params.id;
    Produto.findByPk(id)
        .then(produto => {
            if (!produto) {
                return res.status(404).json({ message: 'Produto não encontrado!' });
            }
            return Produto.destroy({
                where: {
                    id: id
                }
            });
        })
        .then(result => {
            res.status(200).json({ message: 'Produto deletado!' });
        }).catch(err => console.log(err));
}