const Pessoa = require('../models/pessoa');

//Buscar todas as pessoas
exports.getPessoas = (req, res, next) => {
    Pessoa.findAll().then(pessoas => {
        res.status(200).json({ pessoas: pessoas });
    }).catch(err => console.log(err));
}

//Buscar uma pessoa pelo id
exports.getPessoa = (req, res, next) => {
    const id = req.params.id;
    Pessoa.findByPk(id).then(pessoa => {
        if (!pessoa) {
            return res.status(404).json({
                message: 'Pessoa não encontrada'
            });
        }
        res.status(200).json({ pessoa: pessoa });
    }).catch(err => console.log(err));
}

//Criar uma pessoa
exports.createPessoa = (req, res, next) => {
    const nome = req.body.nome;
    const email = req.body.email;
    const senha = req.body.senha;

    Pessoa.create({
        nome: nome,
        email: email,
        senha: senha
    }).then(result => {
        console.log("Pessoa criada!");
        res.status(201).json({
            message: 'Pessoa cadastrada com sucesso!',
            pessoa: result
        });
    }).catch(err => {
        console.log(err);
    });
}

//Atualizar pessoa
exports.updatePessoa = (req, res, next) => {
    const id = req.params.id;
    const updatedNome = req.body.nome;
    const updatedEmail = req.body.email;
    const updatedSenha = req.body.senha;

    Pessoa.findByPk(id).then(pessoa => {
        if (!pessoa) {
            return res.status(404).json({
                message: 'Pessoa não encontrada!'
            });
        }
        pessoa.nome = updatedNome;
        pessoa.email = updatedEmail;
        pessoa.senha = updatedSenha;
        return pessoa.save();
    }).then(result => {
        res.status(200).json({
            message: 'Pessoa atualizada!',
            pessoa: result
        });
    }).catch(err => console.log(err));
}

//Excluir uma pessoa
exports.deletePessoa = (req, res, next) => {
    const id = req.params.id;
    Pessoa.findByPk(id)
        .then(pessoa => {
            if (!pessoa) {
                return res.status(404).json({
                    message: 'Pessoa não encontrada!'
                });
            }
            return Pessoa.destroy({
                where: {
                    id: id
                }
            });
        })
        .then(result => {
            res.status(200).json({ message: 'Pessoa deletada!' });
        })
}

exports.login = async (req, res, next) => {
    console.log(req.body);
    const pessoa = await Pessoa.findOne({ where: {email: req.body.email} });
    if (pessoa) {
        if (req.body.senha == pessoa.senha) {
            res.status(200).json({message: 'Usuário encontrado' });
        } else {
            res.status(400).json({ error: "Senha incorreta" });
        }
    } else {
        res.status(404).json({ error: "Usuário não existe" });
    }
}