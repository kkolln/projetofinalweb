const controller = require('../controllers/itemCarrinho');
const router = require('express').Router();

router.get('/listar/:idPessoa', controller.getItensCarrinho);
router.post('/inserir', controller.addItemCarrinho);
router.put('/atualizar/:idPessoa/:idProduto', controller.updateItemCarrinho);
router.delete('/delete/:idPessoa/:idProduto', controller.deleteItemCarrinho);
router.delete('/delete/:idPessoa', controller.deleteItensCarrinho);
module.exports = router;