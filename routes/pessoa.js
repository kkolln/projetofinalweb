const controller = require('../controllers/pessoa');
const router = require('express').Router();

router.get('/listar', controller.getPessoas);
router.get('/ler/:id', controller.getPessoa);
router.post('/inserir', controller.createPessoa);
router.put('/atualizar/:id', controller.updatePessoa);
router.delete('/delete/:id', controller.deletePessoa);
router.post('/login', controller.login);
module.exports = router;