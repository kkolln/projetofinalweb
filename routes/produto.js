const controller = require('../controllers/produto');
const router = require('express').Router();

router.get('/listar', controller.getProdutos);
router.get('/ler/:id', controller.getProduto);
router.post('/inserir', controller.createProduto);
router.put('/atualizar/:id', controller.updateProduto);
router.delete('/delete/:id', controller.deleteProduto);
module.exports = router;